import os
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from pprint import pprint as pp

list_of_files = [doc for doc in os.listdir() if doc.endswith(".txt")]
content_of_files = [open(File).read() for File in list_of_files]

vectorize = lambda text: TfidfVectorizer().fit_transform(text).toarray()
similarity = lambda doc1, doc2: cosine_similarity([doc1, doc2])

vectors = vectorize(content_of_files)
files_vectors = list(zip(list_of_files, vectors))


def check_plagiarism():
    results = dict()
    for file1, vectorized_file1 in files_vectors:
        for file2, vectorized_file2 in files_vectors:

            if file1 != file2:
                sim_score = similarity(vectorized_file1, vectorized_file2)[0][1]

                if f"{file1} and {file2}" and f"{file2} and {file1}" not in str(results):
                    results[f"{file1} and {file2}"] = sim_score
    return results


pp(check_plagiarism(), width=1)
